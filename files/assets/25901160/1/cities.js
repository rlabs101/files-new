var Cities = pc.createScript('cities');

Cities.attributes.add("sceneId1", {type: "string", default: "0", title: "1"});
Cities.attributes.add("sceneId2", {type: "string", default: "0", title: "2"});
Cities.attributes.add("sceneId3", {type: "string", default: "0", title: "3"});


// initialize code called once per entity
Cities.prototype.initialize = function() {
    
    this.check1 = false;
    this.check2 = false;
    this.check3 = false;
    
};

// update code called every frame
Cities.prototype.update = function(dt) {
    
    var btn1 = this.app.root.findByName('btn1');
    var btn2 = this.app.root.findByName('2');
    var btn3 = this.app.root.findByName('3');
    
    btn1.element.on('click', function (event) {
        if (this.check1 === false) {
            this.changeScenes(this.sceneId1);
            this.check1 = true;
        }
    }, this);
    
    btn2.element.on('click', function (event) {
        if (this.check2 === false) {
            this.changeScenes(this.sceneId2);
            this.check2 = true;
        }
    }, this);
    
    btn3.element.on('click', function (event) {
        if (this.check3 === false) {
            this.changeScenes(this.sceneId3);
            this.check3 = true;
        }
    }, this);
    
};

Cities.prototype.changeScenes = function(scene) {
    // Get a reference to the current root object
    var oldHierarchy = this.app.root.findByName ('Root');
    
    // Load the new scene. The scene ID is found by loading the scene in the editor and 
    // taking the number from the URL
    // e.g. If the URL when Scene 1 is loaded is: https://playcanvas.com/editor/scene/475211
    // The ID is the number on the end (475211)
    this.loadScene (scene, function () {
        // Once the new scene has been loaded, destroy the old one
        oldHierarchy.destroy ();
    });
};

Cities.prototype.loadScene = function (id, callback) {
    // Get the path to the scene
    var url = id  + ".json";
    
    // Load the scenes entity hierarchy
    this.app.loadSceneHierarchy(url, function (err, parent) {
        if (!err) {
            callback(parent);
        } else {
            console.error (err);
        }
    });
};

// swap method called for script hot-reloading
// inherit your script state here
// Cities.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/